package de.fromm.sulver.board

import cats.syntax.show.*
import de.fromm.sulver.failure.*
import de.fromm.sulver.geometry.Coordinates
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class BoardSpec extends AnyFlatSpec with Matchers {
  behavior of "Board.generate"

  it should "initialize a new board as a fully unsolved board" in {
    val boxLength = 3
    val boardLength = boxLength * boxLength
    val board = Board.generate(boxLength).unsafeValue

    board.isSolved shouldBe false
    board.unsolvedCells.length shouldBe boardLength * boardLength
    board.unsolvedCells.foreach {
      _.candidates.length shouldBe boardLength
    }
  }

  behavior of "Board.solveCell"

  it should "suppress codes in neighbor cells after solving" in {
    // given
    val board = Board.generate(2).unsafeValue
    val coordinates = Coordinates.fromValues(0, 0).unsafeValue

    // when
    val solvedBoard = board.solveCell(coordinates, board.codes.head).unsafeValue

    // then
    val solvedCell = solvedBoard.cellAt(coordinates).unsafeValue
    solvedCell.isSolved shouldBe true
    solvedBoard.unsolvedCells.length shouldBe 15
    solvedBoard.unsolvedCells should not contain solvedCell

    {
      import de.fromm.sulver.board.instances.plaintext.debug.*
      solvedBoard.show shouldBe """+-----+-----++-----+-----+
                                  || 1   |   2 ||   2 |   2 |
                                  ||     | 3 4 || 3 4 | 3 4 |
                                  |+-----+-----++-----+-----+
                                  ||   2 |   2 || 1 2 | 1 2 |
                                  || 3 4 | 3 4 || 3 4 | 3 4 |
                                  |+-----+-----++-----+-----+
                                  |+-----+-----++-----+-----+
                                  ||   2 | 1 2 || 1 2 | 1 2 |
                                  || 3 4 | 3 4 || 3 4 | 3 4 |
                                  |+-----+-----++-----+-----+
                                  ||   2 | 1 2 || 1 2 | 1 2 |
                                  || 3 4 | 3 4 || 3 4 | 3 4 |
                                  |+-----+-----++-----+-----+
                                  |""".stripMargin
    }

    {
      import de.fromm.sulver.board.instances.plaintext.*
      solvedBoard.show shouldBe """+---+---++---+---+
                                  || 1 |   ||   |   |
                                  |+---+---++---+---+
                                  ||   |   ||   |   |
                                  |+---+---++---+---+
                                  |+---+---++---+---+
                                  ||   |   ||   |   |
                                  |+---+---++---+---+
                                  ||   |   ||   |   |
                                  |+---+---++---+---+
                                  |""".stripMargin
    }
  }
}
