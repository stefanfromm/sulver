package de.fromm.sulver.failure

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class ApplicationFailureSpec extends AnyFlatSpec with Matchers {
  behavior of "ApplicationFailure.toString"

  it should "return message if no cause is set" in {
    // given
    val failure = InvalidArgumentFailure("foo")

    // when
    val toString = failure.toString

    // then
    toString shouldBe "foo"
  }

  it should "return message if cause is set" in {
    // given
    val failure = InvalidArgumentFailure("foo", Some(InvalidArgumentFailure("bar")))

    // when
    val toString = failure.toString

    // then
    toString shouldBe "foo bar"
  }

  it should "return message if longer cause chain is set" in {
    // given
    val failure =
      InvalidArgumentFailure("foo", Some(InvalidArgumentFailure("bar", Some(InvalidArgumentFailure("baz")))))

    // when
    val toString = failure.toString

    // then
    toString shouldBe "foo bar baz"
  }
}
