package de.fromm.sulver.cats.effect

import cats.instances.future.*
import cats.syntax.traverse.*
import cats.effect.IO
import cats.effect.unsafe.IORuntime
import org.scalatest.Inspectors
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import java.util.concurrent.atomic.AtomicReference
import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.duration.*

type Action = String
type ThreadName = String

class IOSpec extends AnyFlatSpec with Matchers with Inspectors {
  behavior of "IO"

  it should "run synchronously on compute pool by default" in new BaseFixture {
    val threadNames = (0 to 1000).toList.traverse(getThread).unsafeRunSync().map(_._2).toSet
    forAll(threadNames) { _ should startWith("io-compute-") }
    threadNames.size shouldBe 1
  }

  it should "run asynchronously on compute pool by default" in new BaseFixture {
    val threadNames = Await.result((0 to 1000).toList.traverse(getThread).unsafeToFuture(), 5.seconds).map(_._2).toSet
    forAll(threadNames) { _ should startWith("io-compute-") }
    threadNames.size shouldBe 1
  }

  it should "run concurrently on compute pool by default" in new BaseFixture {
    val threadNames =
      Await.result((0 to 1000).toList.map(getThread).map(_.unsafeToFuture()).sequence, 5.seconds).map(_._2).toSet
    println(threadNames)
    forAll(threadNames) { _ should startWith("io-compute-") }
    threadNames.size should be >= 1
    threadNames.size should be <= Runtime.getRuntime.availableProcessors()
  }

  it should "catch exceptions" in new BaseFixture {
    val io = IO[Int] { throw new IllegalArgumentException("Argument is invalid") }.map(_ + 1)
    val exception = the[IllegalArgumentException] thrownBy { io.unsafeRunSync() }
    exception.getMessage shouldBe "Argument is invalid"
  }

  private trait BaseFixture {
    implicit val defaultRuntime: IORuntime = IORuntime.global
    implicit val defaultExecutionContext: ExecutionContext = ExecutionContext.global

    def getThread(action: Int): IO[(Action, ThreadName)] =
      getThread(action.toString)

    def getThread(action: String = "action"): IO[(Action, ThreadName)] =
      IO((action, Thread.currentThread.getName))
  }
}
