package de.fromm.sulver.challenge.parser

import cats.data.{NonEmptyList, NonEmptySet}
import cats.syntax.show.*
import cats.syntax.traverse.*
import de.fromm.sulver.board.instances.plaintext.*
import de.fromm.sulver.failure.*
import de.fromm.sulver.symbols.Code
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class PlainTextChallengeParserSpec extends AnyFlatSpec with Matchers {
  behavior of "PlainTextChallengeParser.parse"

  it should "initialize a new board" in new BaseFixture {
    // given
    val boardLayout = """+---+---+---++---+---+---++---+---+---+
                        || 2 |   |   ||   | 4 |   || 7 |   |   |
                        |+---+---+---++---+---+---++---+---+---+
                        ||   | 9 | 1 ||   |   |   || 6 | 8 | 5 |
                        |+---+---+---++---+---+---++---+---+---+
                        ||   | 3 |   ||   |   |   ||   |   |   |
                        |+---+---+---++---+---+---++---+---+---+
                        |+---+---+---++---+---+---++---+---+---+
                        ||   | 2 | 4 ||   |   |   || 8 |   |   |
                        |+---+---+---++---+---+---++---+---+---+
                        ||   |   |   ||   |   |   ||   | 5 | 2 |
                        |+---+---+---++---+---+---++---+---+---+
                        ||   | 1 |   ||   | 5 |   ||   | 6 |   |
                        |+---+---+---++---+---+---++---+---+---+
                        |+---+---+---++---+---+---++---+---+---+
                        ||   |   |   ||   |   | 8 ||   | 9 |   |
                        |+---+---+---++---+---+---++---+---+---+
                        ||   |   |   ||   | 3 |   || 4 | 7 |   |
                        |+---+---+---++---+---+---++---+---+---+
                        ||   |   |   || 1 |   | 6 || 3 |   |   |
                        |+---+---+---++---+---+---++---+---+---+
                        |""".stripMargin

    // when
    val board = (for {
      challenge <- parser.parse(PlainTextChallenge(boardLayout))
      board <- challenge.toBoard
    } yield board).unsafeValue

    // then
    board.show shouldBe boardLayout
  }

  private trait BaseFixture {
    val parser = PlainTextChallengeParser(
      NonEmptySet.of("1", "2", "3", "4", "5", "6", "7", "8", "9"),
      NonEmptySet.of(" ")
    ).unsafeValue
  }

}
