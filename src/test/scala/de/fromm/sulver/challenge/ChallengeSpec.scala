package de.fromm.sulver.challenge

import cats.data.{NonEmptyList, NonEmptySet}
import cats.syntax.show.*
import cats.syntax.traverse.*
import de.fromm.sulver.board.instances.plaintext.*
import de.fromm.sulver.challenge
import de.fromm.sulver.failure.*
import de.fromm.sulver.geometry.Coordinates
import de.fromm.sulver.symbols.Code
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class ChallengeSpec extends AnyFlatSpec with Matchers {
  behavior of "Challenge.generate"

  it should "initialize a new board" in new BaseFixture {
    // given
    private val knownCells = Map(
      //format: off
      (1, 1) -> 2, (1, 5) -> 4, (1, 7) -> 7, // row 1
      (2, 2) -> 9, (2, 3) -> 1, (2, 7) -> 6, (2, 8) -> 8, (2, 9) -> 5, // row 2
      (3, 2) -> 3, // row 3
      (4, 2) -> 2, (4, 3) -> 4, (4, 7) -> 8, // row 4
      (5, 8) -> 5, (5, 9) -> 2, // row 5
      (6, 2) -> 1, (6, 5) -> 5, (6, 8) -> 6, // row 6
      (7, 6) -> 8, (7, 8) -> 9, // row 7
      (8, 5) -> 3, (8, 7) -> 4, (8, 8) -> 7, // row 8
      (9, 4) -> 1, (9, 6) -> 6, (9, 7) -> 3, // row 9
      //format: on
    )
    // when
    private val board = (for {
      challenge <- Challenge.generate(codes, known(knownCells))
      board <- challenge.toBoard
    } yield board).unsafeValue

    // then
    board.show shouldBe """+---+---+---++---+---+---++---+---+---+
                          || 2 |   |   ||   | 4 |   || 7 |   |   |
                          |+---+---+---++---+---+---++---+---+---+
                          ||   | 9 | 1 ||   |   |   || 6 | 8 | 5 |
                          |+---+---+---++---+---+---++---+---+---+
                          ||   | 3 |   ||   |   |   ||   |   |   |
                          |+---+---+---++---+---+---++---+---+---+
                          |+---+---+---++---+---+---++---+---+---+
                          ||   | 2 | 4 ||   |   |   || 8 |   |   |
                          |+---+---+---++---+---+---++---+---+---+
                          ||   |   |   ||   |   |   ||   | 5 | 2 |
                          |+---+---+---++---+---+---++---+---+---+
                          ||   | 1 |   ||   | 5 |   ||   | 6 |   |
                          |+---+---+---++---+---+---++---+---+---+
                          |+---+---+---++---+---+---++---+---+---+
                          ||   |   |   ||   |   | 8 ||   | 9 |   |
                          |+---+---+---++---+---+---++---+---+---+
                          ||   |   |   ||   | 3 |   || 4 | 7 |   |
                          |+---+---+---++---+---+---++---+---+---+
                          ||   |   |   || 1 |   | 6 || 3 |   |   |
                          |+---+---+---++---+---+---++---+---+---+
                          |""".stripMargin
  }

  private trait BaseFixture {
    protected val codes: NonEmptySet[Code] =
      NonEmptyList.fromListUnsafe((1 to 9).toList.traverse(i => Code.fromValue(i.toString)).unsafeValue).toNes

    def known(codesByCoordinates: Map[(Int, Int), Int]): NonEmptyList[PresolvedCell] =
      codesByCoordinates.toList
        .traverse { case ((row, column), code) =>
          for {
            validatedCoordinates <- Coordinates.fromValues(column - 1, row - 1)
            validatedCode <- Code.fromValue(code.toString)
          } yield challenge.PresolvedCell(validatedCoordinates, validatedCode)
        }
        .flatMap(NonEmptyList.fromList(_).toRight(InvalidArgumentFailure(s"No coordinates given.")))
        .unsafeValue
  }
}
