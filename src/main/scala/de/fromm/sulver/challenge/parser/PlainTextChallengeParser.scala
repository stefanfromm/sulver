package de.fromm.sulver.challenge.parser
import cats.data.{NonEmptyList, NonEmptySet}
import cats.syntax.traverse.*
import de.fromm.sulver.challenge.parser.PlainTextChallengeParser.codeCharactersOf
import de.fromm.sulver.challenge.{Challenge, PresolvedCell}
import de.fromm.sulver.failure.{FailureOr, InvalidArgumentFailure}
import de.fromm.sulver.geometry.Coordinates
import de.fromm.sulver.symbols.Code

import scala.annotation.tailrec
import scala.util.matching.Regex

class PlainTextChallengeParser private (val codes: NonEmptySet[String], val emptyCodes: NonEmptySet[String])
    extends ChallengeParser[PlainTextChallenge] {

  override def parse(in: PlainTextChallenge): FailureOr[Challenge] =
    for {
      parsedLines <- parseLines(in)
      validatedLines <- validateParsedLines(parsedLines)
      challenge <- generateChallenge(validatedLines)
    } yield challenge

  private def validateParsedLines(
      lines: List[NonEmptyList[Option[String]]]
  ): FailureOr[NonEmptyList[NonEmptyList[Option[String]]]] = {
    val boardLength = lines.length

    if (lines.isEmpty)
      Left(InvalidArgumentFailure(s"There must be at least one line."))
    else if (lines.exists(_.length != boardLength))
      Left(
        InvalidArgumentFailure(
          s"Expected $boardLength codes on each line, but at least one line has another amount of codes."
        )
      )
    else if (lines.forall(_.forall(_.isEmpty))) {
      Left(InvalidArgumentFailure("There must be given at least one known code."))
    } else Right(NonEmptyList.fromListUnsafe(lines))
  }

  private def parseLines(in: PlainTextChallenge): FailureOr[List[NonEmptyList[Option[String]]]] =
    for {
      splittingRegex <- generateSplittingRegex
      lines = splitLines(in, splittingRegex)
      parsedLines <- toCodeValues(lines)
    } yield parsedLines

  private def splitLines(in: PlainTextChallenge, splittingRegex: Regex): List[NonEmptyList[String]] =
    in.value.linesIterator.map(splitLine(_, splittingRegex)).collect { case Some(lines) => lines }.toList

  private def splitLine(line: String, splittingRegex: Regex): Option[NonEmptyList[String]] = {
    val columns = splittingRegex.split(line).filterNot(_.isEmpty).toList
    if (columns.nonEmpty) Some(NonEmptyList.fromListUnsafe(columns))
    else None
  }

  private def generateSplittingRegex: FailureOr[Regex] = {
    for {
      codeCharacters <- codeCharactersOf(codes)
      emptyCodeCharacters <- codeCharactersOf(emptyCodes)
    } yield ("[^" + (codeCharacters union emptyCodeCharacters).toNonEmptyList.toList.mkString
      .replace("[", "\\[")
      .replace("]", "\\]")
      .replace("^", "\\^")
      .replace("-", "\\-") + "]+").r
  }

  private def toCodeValues(
      lines: List[NonEmptyList[String]]
  ): FailureOr[List[NonEmptyList[Option[String]]]] =
    lines.traverse(line => line.traverse(toCodeValue))

  private def toCodeValue(value: String): FailureOr[Option[String]] = {
    def normalizeValue(value: String): String = {
      val trimmedValue = value.trim
      if (trimmedValue.nonEmpty) trimmedValue
      else normalizeWhitespaceValue(value)
    }

    @tailrec
    def normalizeWhitespaceValue(value: String): String = {
      val normalizedValue = value.replace("  ", " ")
      if (normalizedValue == value) value
      else normalizeWhitespaceValue(normalizedValue)
    }

    val normalizedValue = normalizeValue(value)
    if (codes.contains(normalizedValue)) Right(Some(normalizedValue))
    else if (emptyCodes.contains(normalizedValue)) Right(None)
    else Left(InvalidArgumentFailure(s"Value '$value' is neither a recognized code nor a recognized empty code."))
  }

  private def generateChallenge(
      lines: NonEmptyList[NonEmptyList[Option[String]]]
  ): FailureOr[Challenge] =
    for {
      knownCells <- generateKnownCells(lines)
      codes <- generateCodes(this.codes)
      challenge <- Challenge.generate(codes, knownCells)
    } yield challenge

  private def generateKnownCells(
      lines: NonEmptyList[NonEmptyList[Option[String]]]
  ): FailureOr[NonEmptyList[PresolvedCell]] = {
    val codesByIndex = for {
      (row, rowIndex) <- lines.toList.map(_.zipWithIndex).zipWithIndex
      (code, columnIndex) <- row.toList
    } yield ((rowIndex, columnIndex), code)

    val result = codesByIndex.traverse { case ((rowIndex, columnIndex), code) =>
      for {
        coordinates <- Coordinates.fromValues(columnIndex, rowIndex)
        code <- code.traverse(Code.fromValue)
      } yield code.map(PresolvedCell(coordinates, _))
    }

    result.map(knownCells => NonEmptyList.fromListUnsafe(knownCells.flatMap(_.toList)))
  }

  private def generateCodes(codes: NonEmptySet[String]): FailureOr[NonEmptySet[Code]] =
    codes.toNonEmptyList.traverse(Code.fromValue).map(_.toNes)
}

object PlainTextChallengeParser {
  def apply(
      codes: NonEmptySet[String],
      emptyCodes: NonEmptySet[String]
  ): FailureOr[PlainTextChallengeParser] =
    for {
      codeCharacters <- codeCharactersOf(codes)
      emptyCodeCharacters <- codeCharactersOf(emptyCodes)
      _ <- validateNoIntersection(codeCharacters, emptyCodeCharacters)
    } yield new PlainTextChallengeParser(codes, emptyCodes)

  private def validateNoIntersection(
      codeCharacters: NonEmptySet[Char],
      emptyCodeCharacters: NonEmptySet[Char]
  ): FailureOr[Unit] =
    if (codeCharacters.intersect(emptyCodeCharacters).nonEmpty)
      Left(
        InvalidArgumentFailure(
          s"Characters of codes for filled cells and empty cells must not intersect with each other."
        )
      )
    else Right(())

  private def codeCharactersOf(codes: NonEmptySet[String]): FailureOr[NonEmptySet[Char]] =
    codes.toNonEmptyList.toList
      .traverse(codeCharactersOf(_).map(_.toNonEmptyList.toList))
      .map(list => NonEmptyList.fromListUnsafe(list.flatten).toNes)

  private def codeCharactersOf(code: String): FailureOr[NonEmptySet[Char]] =
    if (code == null) Left(InvalidArgumentFailure(s"Code must not be null."))
    else if (code.isEmpty) Left(InvalidArgumentFailure(s"Code must not be empty."))
    else Right(NonEmptyList.fromListUnsafe(code.iterator.toList).toNes)

}
