package de.fromm.sulver.challenge.parser

import de.fromm.sulver.challenge.Challenge
import de.fromm.sulver.failure.FailureOr

trait ChallengeParser[-A] {
  def parse(in: A): FailureOr[Challenge]
}
