package de.fromm.sulver.challenge.parser

opaque type PlainTextChallenge = String

object PlainTextChallenge {
  def apply(value: String): PlainTextChallenge = value

  extension (self: PlainTextChallenge) {
    def value: String = self
  }
}
