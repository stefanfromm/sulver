package de.fromm.sulver.challenge

import de.fromm.sulver.symbols.Code
import de.fromm.sulver.geometry.Coordinates

case class PresolvedCell(coordinates: Coordinates, code: Code)
