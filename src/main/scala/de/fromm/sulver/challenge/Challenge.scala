package de.fromm.sulver.challenge

import cats.data.{NonEmptyList, NonEmptySet}
import de.fromm.sulver.board.Board
import de.fromm.sulver.challenge
import de.fromm.sulver.failure.*
import de.fromm.sulver.geometry.Length
import de.fromm.sulver.symbols.Code

case class Challenge private (boxLength: Length, codes: NonEmptySet[Code], knownCells: NonEmptyList[PresolvedCell]) {
  def toBoard: FailureOr[Board] =
    knownCells.foldLeft(Board.generate(boxLength, codes)) { (result, knownCell) =>
      for {
        board <- result
        nextBoard <- board.solveCell(knownCell.coordinates, knownCell.code)
      } yield nextBoard
    }
}

object Challenge {
  def generate(codes: NonEmptySet[Code], knownCells: NonEmptyList[PresolvedCell]): FailureOr[Challenge] = {
    val boardLength = Length.fromValue(codes.length).unsafeValue
    for {
      boxLength <- generateBoxLength(boardLength)
      _ <- validateNumberOfKnownCells(knownCells.length, boardLength)
      _ <- validateCoordinatesOfKnownCellsAreOnBoard(knownCells, boardLength)
      _ <- validateKnownCellsHaveValidCodes(knownCells, codes)
      _ <- validateCoordinatesOfKnownCellsAreUnique(knownCells)
    } yield challenge.Challenge(boxLength, codes, knownCells)
  }

  private def generateBoxLength(desiredBoardLength: Length): FailureOr[Length] = {
    // http://spidertech.iblogger.org/determine-if-an-integers-square-root-is-an-integer-in-java/?i=1
    val sqrt = Math.sqrt(desiredBoardLength.value.toDouble)
    val intSqrt = (sqrt + 0.5).toInt
    Option
      .when(intSqrt * intSqrt == desiredBoardLength.value)(intSqrt)
      .toRight(
        InvalidArgumentFailure(
          s"The desired board length $desiredBoardLength does not allow to form quadratic boxes of the same length."
        )
      )
      .flatMap(Length.fromValue)
  }

  private def validateNumberOfKnownCells(
      numberOfKnownCells: Int,
      boardLength: Length
  ): FailureOr[Unit] = {
    val availableNumberOfCells = boardLength.square
    if (numberOfKnownCells > availableNumberOfCells.value)
      Left(
        InvalidArgumentFailure(
          s"The number of known cells $numberOfKnownCells would exceed the available number of cells $availableNumberOfCells."
        )
      )
    else Right(())
  }

  private def validateCoordinatesOfKnownCellsAreOnBoard(
      knownCells: NonEmptyList[PresolvedCell],
      boardLength: Length
  ): FailureOr[Unit] = {
    val knownCellsOutsideOfBoard = knownCells.filter(knownCell =>
      knownCell.coordinates.column.value >= boardLength.value || knownCell.coordinates.row.value >= boardLength.value
    )

    if (knownCellsOutsideOfBoard.nonEmpty)
      Left(
        InvalidArgumentFailure(
          s"The known cells $knownCellsOutsideOfBoard have coordinates outside of the board with side lengths $boardLength."
        )
      )
    else Right(())
  }

  private def validateCoordinatesOfKnownCellsAreUnique(
      knownCells: NonEmptyList[PresolvedCell]
  ): FailureOr[Unit] = {
    val coordinates = knownCells.map(_.coordinates).toList
    val nonUniqueCoordinates = coordinates diff coordinates.distinct

    if (nonUniqueCoordinates.nonEmpty)
      Left(
        InvalidArgumentFailure(
          s"For the coordinates $nonUniqueCoordinates there are given more than one known cell: ${knownCells
            .filter(knownCell => nonUniqueCoordinates.contains(knownCell.coordinates))}."
        )
      )
    else Right(())
  }

  private def validateKnownCellsHaveValidCodes(
      knownCells: NonEmptyList[PresolvedCell],
      codes: NonEmptySet[Code]
  ): FailureOr[Unit] = {
    val knownCellsWithInvalidCodes = knownCells.filterNot(knownCell => codes.contains(knownCell.code))
    if (knownCellsWithInvalidCodes.nonEmpty)
      Left(
        InvalidArgumentFailure(
          s"Known cells $knownCellsWithInvalidCodes contain codes which are not in the set of available codes $codes."
        )
      )
    else Right(())
  }
}
