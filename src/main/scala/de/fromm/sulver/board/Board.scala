package de.fromm.sulver.board

import cats.Order
import cats.data.{NonEmptyList, NonEmptySet}
import cats.syntax.traverse.*
import de.fromm.sulver.board
import de.fromm.sulver.failure.{FailureOr, InvalidArgumentFailure}
import de.fromm.sulver.geometry.{Coordinates, Length, Position}
import de.fromm.sulver.symbols.Code

import scala.collection.immutable.SortedSet

class Board private (val boxLength: Length, val codes: NonEmptySet[Code], unsortedCells: Iterable[Cell]) {
  lazy val boardLength: Length =
    boxLength.square

  private val cells: IndexedSeq[Cell] = unsortedCells.toIndexedSeq.sorted

  def rows: NonEmptyList[Row] = {
    val result: Seq[Row] = for (row <- boardLength.allPositions) yield unsafeRowAt(row)
    NonEmptyList.fromListUnsafe(result.toList)
  }

  def rowAt(position: Position): Option[Row] =
    if (boardLength.contains(position)) Some(unsafeRowAt(position))
    else None

  private def unsafeRowAt(row: Position): Row = {
    val startIndex = cellIndexOf(Position.first, row)
    val endIndex = cellIndexOf(Position.first, row.next)
    Row(row, NonEmptyList.fromListUnsafe(cells.slice(startIndex, endIndex).toList))
  }

  def columns: NonEmptyList[Column] = {
    val result: Seq[Column] =
      for (column <- boardLength.allPositions) yield unsafeColumnAt(column)
    NonEmptyList.fromListUnsafe(result.toList)
  }

  def columnAt(position: Position): Option[Column] =
    if (boardLength.contains(position)) Some(unsafeColumnAt(position))
    else None

  private def unsafeColumnAt(column: Position): Column = {
    val columnCells: Seq[Cell] = for {
      row <- boardLength.allPositions
      cellIndex = cellIndexOf(column, row)
    } yield cells(cellIndex)

    Column(column, NonEmptyList.fromListUnsafe(columnCells.toList))
  }

  private def cellIndexOf(column: Position, row: Position): Int =
    row.value * boardLength.value + column.value

  def boxes: NonEmptyList[Box] = {
    val result = for {
      row <- boardLength allPositions boxLength
      column <- boardLength allPositions boxLength
    } yield boxAt(column, row)

    NonEmptyList.fromListUnsafe(result.toList)
  }

  def isSolved: Boolean =
    cells.forall(_.isSolved)

  def unsolvedCells: List[Cell] =
    cells.filterNot(_.isSolved).toList

  def solveCell(coordinates: Coordinates, code: Code): FailureOr[Board] =
    for {
      cellToSolve <- cellAt(coordinates)
      solvedCell <- cellToSolve.solveTo(code)
      cellsToSuppress = findCellsToSuppress(cellToSolve, code)
      suppressedCells <- cellsToSuppress.traverse(_.suppress(code))
      cellsToKeep = cells.diff(cellToSolve :: cellsToSuppress).toList
    } yield new Board(
      boxLength = boxLength,
      codes = codes,
      unsortedCells = solvedCell :: suppressedCells ::: cellsToKeep
    )

  def suppressCode(coordinates: Coordinates, code: Code): FailureOr[Board] =
    for {
      cellToSuppress <- cellAt(coordinates)
      suppressedCell <- cellToSuppress.suppress(code)
      cellsToKeep = cells.diff(List(cellToSuppress)).toList
    } yield new Board(
      boxLength = boxLength,
      codes = codes,
      unsortedCells = suppressedCell :: cellsToKeep
    )

  private def findCellsToSuppress(solvedCell: Cell, code: Code): List[Cell] = {
    val cellCandidates =
      unsafeRowAt(solvedCell.row).cells ::: unsafeColumnAt(solvedCell.column).cells ::: boxAt(solvedCell).cells
    cellCandidates.filter(cell => cell.candidates.contains(code) && cell != solvedCell).distinct
  }

  private def boxAt(cell: Cell): Box =
    boxAt(cell.coordinates.column, cell.coordinates.row)

  private def boxAt(column: Position, row: Position): Box = {
    val boxStartRow = row.boxStartPosition(boxLength)
    val boxStartColumn = column.boxStartPosition(boxLength)

    val boxCells: IndexedSeq[Cell] = for {
      row <- boxStartRow rangeBy boxLength
      column <- boxStartColumn rangeBy boxLength
    } yield cells(cellIndexOf(column, row))

    Box(
      Coordinates(column = boxStartColumn / boxLength, row = boxStartRow / boxLength),
      NonEmptyList.fromListUnsafe(boxCells.toList)
    )
  }

  def cellAt(coordinates: Coordinates): FailureOr[Cell] =
    if (coordinates.row.value >= boardLength.value)
      Left(InvalidArgumentFailure(s"Row must not exceed board length $boardLength, but was ${coordinates.row}."))
    else if (coordinates.column.value >= boardLength.value)
      Left(InvalidArgumentFailure(s"Column must not exceed board length $boardLength, but was ${coordinates.column}."))
    else
      Right(cells(cellIndexOf(coordinates.column, coordinates.row)))
}

object Board {
  def generate(boxLength: Int): FailureOr[Board] = {
    for {
      validatedBoxLength <- Length
        .fromValue(boxLength)
        .left
        .map(failure => InvalidArgumentFailure(s"Invalid box length. $failure"))
      codes <- generateCodes(validatedBoxLength.square)
      board <- generate(validatedBoxLength, codes)
    } yield board
  }

  def generate(boxLength: Length, codes: NonEmptySet[Code]): FailureOr[Board] = {
    // todo: check that box length is at least 2?
    if (boxLength.square.value != codes.length)
      Left(
        InvalidArgumentFailure(
          s"For a box length of $boxLength there is expected ${boxLength.square} codes, but there was given ${codes.length} codes."
        )
      )
    else generateBoard(boxLength, codes)
  }

  private def generateBoard(boxLength: Length, codes: NonEmptySet[Code]): FailureOr[Board] =
    for {
      coordinates <- generateCoordinates(boxLength.square)
      cells = coordinates.map(Cell(_, codes, isSolved = false))
    } yield Board(boxLength, codes, cells)

  private def generateCoordinates(boardLength: Length): FailureOr[Seq[Coordinates]] = {
    val coordinates: Seq[FailureOr[Coordinates]] = for {
      row <- 0 until boardLength.value
      column <- 0 until boardLength.value
    } yield Coordinates.fromValues(column, row)

    coordinates.sequence
  }

  private def generateCodes(
      boardLength: Length
  )(implicit order: Order[Code]): FailureOr[NonEmptySet[Code]] =
    (1 to boardLength.value)
      .map(code => Code.fromValue(code.toString))
      .toList
      .sequence
      .map(codes => NonEmptySet.fromSetUnsafe(SortedSet.from(codes)(order.toOrdering)))
}
