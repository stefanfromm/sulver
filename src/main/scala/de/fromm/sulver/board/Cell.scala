package de.fromm.sulver.board

import cats.Order
import cats.data.NonEmptySet
import de.fromm.sulver.failure.{FailureOr, InvalidArgumentFailure}
import de.fromm.sulver.geometry.{Coordinates, Position}
import de.fromm.sulver.symbols.Code

case class Cell(
    coordinates: Coordinates,
    candidates: NonEmptySet[Code],
    isSolved: Boolean
) {
  def row: Position =
    coordinates.row

  def column: Position =
    coordinates.column

  def solvedCode: Option[Code] =
    if (isSolved) Some(candidates.head) else None

  override def toString: String =
    coordinates.toString

  def solveTo(code: Code): FailureOr[Cell] = {
    if (isSolved) Left(InvalidArgumentFailure(s"Cell $this is already solved."))
    else if (!candidates.contains(code))
      Left(InvalidArgumentFailure(s"Cell $this cannot be solved with $code. It allows only for $candidates."))
    else Right(copy(candidates = NonEmptySet.one(code), isSolved = true))
  }

  def suppress(code: Code): FailureOr[Cell] = {
    if (isSolved) Left(InvalidArgumentFailure(s"Cell $this is already solved."))
    else if (!candidates.contains(code))
      Left(InvalidArgumentFailure(s"Code $code cannot be suppressed in cell $this. It allows only for $candidates."))
    else if (candidates.length == 1)
      Left(
        InvalidArgumentFailure(s"Code $code cannot be suppressed in cell $this. It would suppress the last code in it.")
      )
    else Right(copy(candidates = NonEmptySet.fromSetUnsafe(candidates.filterNot(_ == code))))
  }
}

object Cell {
  implicit val ordering: Ordering[Cell] =
    Ordering
      .by((cell: Cell) => cell.coordinates.row.value)
      .orElseBy((cell: Cell) => cell.coordinates.column.value)

  implicit val order: Order[Cell] =
    Order.fromOrdering(ordering)
}
