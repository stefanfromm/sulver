package de.fromm.sulver.board

import cats.data.{NonEmptyList, NonEmptyMap}
import de.fromm.sulver.board.Cell
import de.fromm.sulver.board.Cell.*
import de.fromm.sulver.geometry.{Coordinates, Length, Position}
import de.fromm.sulver.symbols.Code
import de.fromm.sulver.symbols.Code.*

case class Box(coordinates: Coordinates, cells: NonEmptyList[Cell]) {
  def row: Position =
    coordinates.row

  def column: Position =
    coordinates.column
}
