package de.fromm.sulver.board

import cats.Order
import cats.data.NonEmptyList
import de.fromm.sulver.geometry.Position

sealed trait Line {
  def position: Position
  def cells: NonEmptyList[Cell]
}

object Line {
  implicit val order: Order[Line] = Order.by((line: Line) => line.position)
}

case class Row(position: Position, cells: NonEmptyList[Cell]) extends Line
case class Column(position: Position, cells: NonEmptyList[Cell]) extends Line
