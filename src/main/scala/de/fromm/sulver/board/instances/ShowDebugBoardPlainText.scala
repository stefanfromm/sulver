package de.fromm.sulver.board.instances

import cats.Show
import cats.data.NonEmptyList
import cats.syntax.list.*
import de.fromm.sulver.board.instances.ShowDebugBoardPlainText.{SubCell, SubRow}
import de.fromm.sulver.board.{Board, Row}
import de.fromm.sulver.failure.*
import de.fromm.sulver.geometry.Position
import de.fromm.sulver.symbols.Code
import de.fromm.sulver.util.string.*

import scala.util.Properties.lineSeparator

private[instances] class ShowDebugBoardPlainText extends Show[Board] {
  override def show(board: Board): String = {
    val maxCodeLength = board.codes.map(_.toString.length).toNonEmptyList.toList.max
    val separatorRow =
      (("+" + ("-" * (1 + (1 + maxCodeLength) * board.boxLength.value))) * board.boxLength.value + "+") * board.boxLength.value + lineSeparator

    board.rows.flatMap(row => splitRow(board, row)).foldLeft(separatorRow) { (result, nextSubRow) =>
      result + showSubRow(board, nextSubRow, maxCodeLength) + (separatorRow * duplicates(board, nextSubRow))
    }
  }

  private def splitRow(board: Board, row: Row): NonEmptyList[SubRow] = {
    val subRows =
      for (subRowIndex <- 0 until board.boxLength.value)
        yield SubRow(
          rowPosition = row.position,
          subRowPosition = Position.fromValue(subRowIndex).unsafeValue,
          subCells = row.cells.map(cell =>
            SubCell(
              columnPosition = cell.coordinates.column,
              candidates = board.codes.toNonEmptyList.toList
                .slice(subRowIndex * board.boxLength.value, (subRowIndex + 1) * board.boxLength.value)
                .map(availableCode => Option.when(cell.candidates.contains(availableCode))(availableCode))
                .toNel
                .get
            )
          )
        )

    NonEmptyList.fromListUnsafe(subRows.toList)
  }

  private def showSubRow(board: Board, subRow: SubRow, maxCodeLength: Int): String =
    subRow.subCells.foldLeft("|") { (result, subCell) =>
      result + showSubCell(board, subCell, maxCodeLength)
    } + lineSeparator

  private def showSubCell(board: Board, subCell: SubCell, maxCodeLength: Int): String =
    subCell.candidates.foldLeft(" ") { (result, candidate) =>
      result + unsolvedCode(candidate, maxCodeLength) + " "
    } + ("|" * duplicates(board, subCell))

  private def unsolvedCode(code: Option[Code], maxCodeLength: Int): String =
    code.map(_.value).getOrElse(" ").lpadTo(maxCodeLength, ' ')

  private def duplicates(board: Board, subCell: SubCell): Int =
    if (subCell.columnPosition.value == board.boardLength.value - 1) 1
    else if ((subCell.columnPosition.value + 1) % board.boxLength.value == 0) 2
    else 1

  private def duplicates(board: Board, subRow: SubRow): Int = {
    def lastSubRowDuplicates(n: Int): Int =
      if (subRow.subRowPosition.value == board.boxLength.value - 1) n else 0

    if (subRow.rowPosition.value == board.boardLength.value - 1)
      lastSubRowDuplicates(1)
    else if ((subRow.rowPosition.value + 1) % board.boxLength.value == 0)
      lastSubRowDuplicates(2)
    else
      lastSubRowDuplicates(1)
  }
}

private object ShowDebugBoardPlainText {
  case class SubRow(rowPosition: Position, subRowPosition: Position, subCells: NonEmptyList[SubCell])

  case class SubCell(columnPosition: Position, candidates: NonEmptyList[Option[Code]])
}
