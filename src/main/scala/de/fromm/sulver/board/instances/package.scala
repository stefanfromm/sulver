package de.fromm.sulver.board

package object instances {
  object plaintext {
    implicit val showBoard: ShowBoardPlainText = new ShowBoardPlainText

    object debug {
      implicit val showDebugBoard: ShowDebugBoardPlainText = new ShowDebugBoardPlainText
    }
  }
}
