package de.fromm.sulver.board.instances

import cats.Show
import de.fromm.sulver.board.{Board, Cell, Row}
import de.fromm.sulver.geometry.Position
import de.fromm.sulver.util.string.*

import scala.util.Properties.lineSeparator

private[instances] class ShowBoardPlainText extends Show[Board] {
  override def show(board: Board): String = {
    val maxCodeLength = board.codes.map(_.toString.length).toNonEmptyList.toList.max
    val separatorRow =
      (("+" + ("-" * (2 + maxCodeLength))) * board.boxLength.value + "+") * board.boxLength.value + lineSeparator

    board.rows.foldLeft(separatorRow) { (result, row) =>
      result + showRow(board, row, maxCodeLength) + (separatorRow * duplicates(board)(row.position))
    }
  }

  private def showRow(board: Board, row: Row, maxCodeLength: Int): String =
    row.cells.foldLeft("|") { (result, cell) => result + showCell(board)(cell, maxCodeLength) } + lineSeparator

  private def showCell(board: Board)(cell: Cell, maxCodeLength: Int): String =
    " " + solvedCode(cell, maxCodeLength) + " " + ("|" * duplicates(board)(cell.column))

  private def solvedCode(cell: Cell, maxCodeLength: Int): String =
    cell.solvedCode
      .map(_.value.lpadTo(maxCodeLength, ' '))
      .getOrElse(" " * maxCodeLength)

  private def duplicates(board: Board)(position: Position): Int =
    if (position.value == board.boardLength.value - 1) 1
    else if ((position.value + 1) % board.boxLength.value == 0) 2
    else 1
}
