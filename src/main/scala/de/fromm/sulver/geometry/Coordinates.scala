package de.fromm.sulver.geometry

import de.fromm.sulver.failure.{FailureOr, InvalidArgumentFailure}

/** 0-based column and row coordinates.
  *
  * @param column
  *   The column position.
  * @param row
  *   The row position.
  */
case class Coordinates(column: Position, row: Position) {
  override def toString: String =
    s"$column x $row"
}

object Coordinates {
  def fromValues(column: Int, row: Int): FailureOr[Coordinates] =
    for {
      column <- Position
        .fromValue(column)
        .left
        .map(failure => InvalidArgumentFailure("Column is invalid.", Some(failure)))
      row <- Position.fromValue(row).left.map(failure => InvalidArgumentFailure("Row is invalid.", Some(failure)))
    } yield new Coordinates(column, row)
}
