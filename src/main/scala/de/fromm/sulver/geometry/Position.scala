package de.fromm.sulver.geometry

import cats.kernel.Order
import de.fromm.sulver.failure.{FailureOr, InvalidArgumentFailure}

/** 0-based position.
  *
  * @param value
  *   The internal value. At least 0.
  */
case class Position private (value: Int) extends AnyVal {
  def boxStartPosition(boxLength: Length): Position =
    Position(value - value % boxLength.value)

  def /(length: Length): Position =
    Position(value / length.value)

  def next: Position =
    Position(value + 1)

  def rangeBy(added: Length): IndexedSeq[Position] =
    value.until(value + added.value).map(Position.apply)

  def rangeBy(added: Length, step: Length): IndexedSeq[Position] =
    value.until(value + added.value, step.value).map(Position.apply)

  override def toString: String =
    value.toString
}

object Position {
  implicit val order: Order[Position] = Order.by((position: Position) => position.value)

  val first: Position =
    Position(0)

  def fromValue(value: Int): FailureOr[Position] =
    if (value < first.value) Left(InvalidArgumentFailure(s"Position must not be lower than $first, but was $value."))
    else Right(new Position(value))

  def until(exclusiveMax: Length): IndexedSeq[Position] =
    first rangeBy exclusiveMax

  def until(exclusiveMax: Length, step: Length): IndexedSeq[Position] =
    first.rangeBy(exclusiveMax, step)
}
