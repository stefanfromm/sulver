package de.fromm.sulver.geometry

import de.fromm.sulver.failure.{FailureOr, InvalidArgumentFailure}

/** A length. At least 1.
  */
case class Length private (value: Int) extends AnyVal {
  def square: Length =
    Length(value * value)

  def allPositions: IndexedSeq[Position] =
    Position until this

  def allPositions(step: Length): IndexedSeq[Position] =
    Position until (this, step)

  def contains(position: Position): Boolean =
    position.value < value

  override def toString: String =
    value.toString
}

object Length {
  def fromValue(value: Int): FailureOr[Length] =
    if (value < 1) Left(InvalidArgumentFailure(s"Length must not be less than 1, but was $value."))
    else Right(new Length(value))
}
