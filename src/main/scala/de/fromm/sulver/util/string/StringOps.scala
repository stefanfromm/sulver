package de.fromm.sulver.util.string

implicit class StringOps(val self: String) extends AnyVal {

  def lpadTo(length: Int, padding: Char): String =
    if (self.length >= length) self
    else (padding.toString * (length - self.length)) + self
}
