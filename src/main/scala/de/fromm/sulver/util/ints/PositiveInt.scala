package de.fromm.sulver.util.ints

import de.fromm.sulver.failure.{FailureOr, InvalidArgumentFailure}

opaque type PositiveInt = Int

object PositiveInt {
  val MaxValue: PositiveInt = Int.MaxValue

  def apply(i: Int): FailureOr[PositiveInt] =
    if i <= 0 then Left(InvalidArgumentFailure(s"The integer $i was not greater than zero."))
    else Right(i)

  extension (self: PositiveInt) {
    def value: Int = self
  }
}
