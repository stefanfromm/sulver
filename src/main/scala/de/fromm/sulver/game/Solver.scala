package de.fromm.sulver.game

import cats.Eval
import de.fromm.sulver.board.*
import de.fromm.sulver.challenge.Challenge
import de.fromm.sulver.failure.{BoardAlreadySolved, BoardTooDifficult, FailureOr, NoMoreStepsLeft}
import de.fromm.sulver.game.Move.{SolveCell, SuppressCode}
import de.fromm.sulver.util.ints.PositiveInt

import scala.Ordering.Implicits.*

class Solver {
  private val moveLookupPatterns: LazyList[MoveFinder] =
    LazyList(SolvedCellsFinder(), EffectivelySolvedCellsFinder(), SuppressedCodesFinder())

  def solve(challenge: Challenge, maxSteps: PositiveInt): FailureOr[Solution] = for {
    boardToSolve <- challenge.toBoard
    solution <- solve(boardToSolve, maxSteps)
  } yield solution

  private def solve(boardToSolve: Board, maxSteps: PositiveInt): FailureOr[Solution] = {
    def go(board: Board, difficulty: Difficulty, stepsLeft: Int): Eval[FailureOr[(Board, Difficulty)]] =
      (stepsLeft, nextMoves(board)) match {
        case (0, _) =>
          Eval.now(
            if board.isSolved then Right((board, difficulty))
            else Left(NoMoreStepsLeft(boardToSolve, board, difficulty, maxSteps))
          )
        case (_, Nil) =>
          Eval.now(if board.isSolved then Right((board, difficulty)) else Left(BoardTooDifficult(board)))
        case (stepsLeft, moves) =>
          Eval.defer {
            Eval.always {
              val (result, goneSteps) = executeMoves(board, moves, stepsLeft)
              result.flatMap(go(_, maxDifficulty(difficulty, moves), stepsLeft - goneSteps).value)
            }
          }
      }

    if boardToSolve.isSolved then Left(BoardAlreadySolved(boardToSolve))
    else
      go(boardToSolve, Difficulty.Easy, maxSteps.value).value.map { case (solvedBoard, difficulty) =>
        Solution(boardToSolve, solvedBoard, difficulty)
      }
  }

  private def nextMoves(board: Board): List[Move] =
    findNextSetOfMoves(board).find(_.nonEmpty).getOrElse(List.empty).distinct

  private def findNextSetOfMoves(board: Board): LazyList[List[Move]] =
    moveLookupPatterns.map(_.findMoves(board))

  private def executeMoves(board: Board, moves: List[Move], maxSteps: Int): (FailureOr[Board], Int) =
    moves.foldLeft(FailureOr.right(board) -> 0) { case ((result, goneSteps), move) =>
      if goneSteps >= maxSteps then (result, goneSteps)
      else {
        val solvingMoves = if move.isSolvingMove then 1 else 0
        result.flatMap(executeMove(_, move)) -> (goneSteps + solvingMoves)
      }
    }

  private def executeMove(board: Board, move: Move): FailureOr[Board] =
    move match {
      case SolveCell(_, coordinates, code)    => board.solveCell(coordinates, code)
      case SuppressCode(_, coordinates, code) => board.suppressCode(coordinates, code)
    }

  private def maxDifficulty(recentDifficulty: Difficulty, moves: List[Move]): Difficulty =
    recentDifficulty max moves.map(_.difficulty).max
}
