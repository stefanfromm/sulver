package de.fromm.sulver.game

sealed trait Difficulty(val level: Int) extends Ordered[Difficulty] {
  override def compare(that: Difficulty): Int = level - that.level
}

object Difficulty {
  case object Easy extends Difficulty(0)
  case object Medium extends Difficulty(1)
  case object Difficult extends Difficulty(2)
}
