package de.fromm.sulver.game

import de.fromm.sulver.board.Board

case class Solution(boardToSolve: Board, solvedBoard: Board, difficulty: Difficulty)
