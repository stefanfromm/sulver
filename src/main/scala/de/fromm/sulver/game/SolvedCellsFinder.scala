package de.fromm.sulver.game
import de.fromm.sulver.board.Board
import de.fromm.sulver.game.Move.SolveCell

class SolvedCellsFinder extends MoveFinder(Difficulty.Easy) {
  override def findMoves(board: Board): List[Move] =
    board.unsolvedCells
      .filter(_.candidates.length == 1)
      .map(solvableCell => SolveCell(difficulty, solvableCell.coordinates, solvableCell.candidates.head))
}
