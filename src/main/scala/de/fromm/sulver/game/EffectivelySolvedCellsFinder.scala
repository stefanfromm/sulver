package de.fromm.sulver.game
import cats.data.NonEmptyList
import de.fromm.sulver.board.*
import de.fromm.sulver.game.EffectivelySolvedCellsFinder.*
import de.fromm.sulver.game.Move.SolveCell

class EffectivelySolvedCellsFinder extends MoveFinder(Difficulty.Medium) {
  override def findMoves(board: Board): List[Move] =
    (findEffectivelySolvedCellsInCellGroups(
      board,
      board.rows
    ) ::: findEffectivelySolvedCellsInCellGroups(board, board.columns) ::: findEffectivelySolvedCellsInCellGroups(
      board,
      board.boxes
    )).distinct

  private def findEffectivelySolvedCellsInCellGroups[A](
      board: Board,
      cellGroups: NonEmptyList[A]
  )(implicit cells: HasCells[A]): List[Move] = {
    for {
      group <- cellGroups.toList
      code <- board.codes.toNonEmptyList.toList
      cellsWithCode = cells(group).toList.filterNot(_.isSolved).filter(_.candidates.contains(code))
      solvableCell <-
        if (cellsWithCode.length == 1)
          List(SolveCell(difficulty, cellsWithCode.head.coordinates, code))
        else List.empty
    } yield solvableCell
  }
}

private object EffectivelySolvedCellsFinder {
  type HasCells[A] = A => NonEmptyList[Cell]

  implicit val rowCells: HasCells[Row] = _.cells
  implicit val columnCells: HasCells[Column] = _.cells
  implicit val boxCells: HasCells[Box] = _.cells
}
