package de.fromm.sulver.game

import de.fromm.sulver.board.Board

trait MoveFinder(val difficulty: Difficulty) {
  def findMoves(board: Board): List[Move]
}
