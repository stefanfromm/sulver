package de.fromm.sulver.game

import de.fromm.sulver.game.Move.{SolveCell, SuppressCode}
import de.fromm.sulver.geometry.Coordinates
import de.fromm.sulver.symbols.Code

sealed trait Move {
  def difficulty: Difficulty
  
  def isSolvingMove: Boolean = this match {
    case _: SolveCell => true
    case _: SuppressCode => false
  }
}

object Move {
  case class SolveCell(difficulty: Difficulty, coordinates: Coordinates, code: Code) extends Move

  case class SuppressCode(difficulty: Difficulty, coordinates: Coordinates, code: Code) extends Move
}
