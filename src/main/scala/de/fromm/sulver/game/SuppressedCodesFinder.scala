package de.fromm.sulver.game
import cats.data.NonEmptyList
import de.fromm.sulver.board.*
import de.fromm.sulver.game.Move.SuppressCode
import de.fromm.sulver.symbols.Code

class SuppressedCodesFinder extends MoveFinder(Difficulty.Difficult) {
  override def findMoves(board: Board): List[Move] =
    board.boxes.toList.flatMap(findSuppressingGroupsOfCellsInBoxes(board, _)).distinct

  private def findSuppressingGroupsOfCellsInBoxes(board: Board, box: Box): List[Move] =
    for {
      (code, boxCells) <- getUnsolvedCodesInCells(box).toList
      if boxCells.size > 1 && boxCells.size <= board.boxLength.value
      line <- commonLineOf(board, boxCells).toList
      otherCellInLine <- line.cells.toList.diff(boxCells.toList)
      if otherCellInLine.candidates.contains(code)
    } yield SuppressCode(difficulty, otherCellInLine.coordinates, code)

  private def getUnsolvedCodesInCells(box: Box): Map[Code, NonEmptyList[Cell]] =
    box.cells
      .filterNot(_.isSolved)
      .flatMap(cell => cell.candidates.toNonEmptyList.toList.map((_, cell)))
      .groupBy { case (code, _) => code }
      .view
      .mapValues(codesAndCells => NonEmptyList.fromListUnsafe(codesAndCells.map { case (_, cell) => cell }))
      .toMap

  private def commonLineOf(board: Board, cells: NonEmptyList[Cell]): Option[Line] =
    commonRowOf(board, cells).orElse(commonColumnOf(board, cells))

  private def commonRowOf(board: Board, cells: NonEmptyList[Cell]): Option[Row] =
    cells.groupBy(_.row).toList match {
      case (position, cells) :: Nil => board.rowAt(position)
      case _                        => None
    }

  private def commonColumnOf(board: Board, cells: NonEmptyList[Cell]): Option[Column] =
    cells.groupBy(_.column).toList match {
      case (position, cells) :: Nil => board.columnAt(position)
      case _                        => None
    }
}
