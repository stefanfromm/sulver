package de.fromm.sulver.symbols

import cats.Order
import cats.data.NonEmptySet
import de.fromm.sulver.failure.*

import scala.collection.immutable.SortedSet
import scala.util.Try

opaque type Code = String

object Code {
  extension (self: Code) {
    def value: String = self
    def chars: NonEmptySet[Char] = NonEmptySet.fromSetUnsafe(SortedSet.from(self.iterator))
  }

  def fromValue(value: String): FailureOr[Code] =
    if (value == null) Left(InvalidArgumentFailure("Code must not be null."))
    else if (value.trim != value) Left(InvalidArgumentFailure("Code must not contain white space."))
    else if (value.isEmpty) Left(InvalidArgumentFailure("Code must not be empty."))
    else Right(value)

  implicit val order: Order[Code] = new Order[Code] {
    override def compare(x: Code, y: Code): Int = {
      val numericComparison = for {
        xNumber <- toNumber(x)
        yNumber <- toNumber(y)
      } yield xNumber.compareTo(yNumber)

      numericComparison.getOrElse(x.compare(y))
    }

    private def toNumber(code: Code): Option[BigInt] =
      Try(BigInt(code)).toOption
  }
}
