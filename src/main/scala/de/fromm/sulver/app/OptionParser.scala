package de.fromm.sulver.app

import de.fromm.sulver.app.OptionParser.TokenizedOptions
import de.fromm.sulver.failure.{FailureOr, InvalidArgumentFailure}
import de.fromm.sulver.util.ints.PositiveInt

import java.io.File
import scala.annotation.tailrec

class OptionParser {
  def parseOptions(arguments: List[String]): FailureOr[Options] =
    tokenizeOptions(arguments).toOptions

  private def tokenizeOptions(args: List[String]): TokenizedOptions = {
    @tailrec
    def go(restArgs: List[String], accumulator: TokenizedOptions): TokenizedOptions =
      restArgs match {
        case Nil                        => accumulator
        case "--debug" :: tail          => go(tail, accumulator.copy(debug = Some(true)))
        case "--steps" :: steps :: tail => go(tail, accumulator.copy(steps = Some(steps)))
        case boardFile :: tail          => go(tail, accumulator.copy(boardFile = Some(boardFile)))
      }

    go(args, TokenizedOptions())
  }

}

private object OptionParser {
  case class TokenizedOptions(
      debug: Option[Boolean] = None,
      boardFile: Option[String] = None,
      steps: Option[String] = None
  ) {

    def toOptions: FailureOr[Options] =
      for {
        validatedBoardFileName <- validateBoardFileName(boardFile)
        validatedSteps <- validateSteps(steps)
      } yield Options(
        debug = debug.getOrElse(false),
        boardFileName = validatedBoardFileName,
        steps = validatedSteps
      )

    private def validateBoardFileName(boardFileName: Option[String]): FailureOr[String] =
      boardFileName.fold(Left(InvalidArgumentFailure(s"There is no board file name given."))) { boardFileName =>
        if boardFileName.isBlank then Left(InvalidArgumentFailure(s"There is no board file name given."))
        else if !new File(boardFileName).exists then
          Left(InvalidArgumentFailure(s"The given board file does not exist."))
        else if new File(boardFileName).isDirectory then
          Left(InvalidArgumentFailure(s"The given board file is a directory, not a file."))
        else Right(boardFileName)
      }

    private def validateSteps(steps: Option[String]): FailureOr[PositiveInt] =
      steps.fold(Right(PositiveInt.MaxValue)) { steps =>
        steps.toIntOption
          .toRight(InvalidArgumentFailure(s"Steps must be a positive integer number."))
          .flatMap(PositiveInt.apply)
      }
  }
}
