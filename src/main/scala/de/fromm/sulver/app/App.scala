package de.fromm.sulver.app

import cats.data.{NonEmptyList, NonEmptySet}
import cats.syntax.show.*
import cats.syntax.traverse.*
import cats.effect.{ExitCode, IO, IOApp, Resource}
import de.fromm.sulver.board.Board
import de.fromm.sulver.challenge.Challenge
import de.fromm.sulver.failure.*
import de.fromm.sulver.challenge.parser.{PlainTextChallenge, PlainTextChallengeParser}
import de.fromm.sulver.game.{Difficulty, Solution, Solver}

import scala.io.Source
import scala.util.{Properties, Using}

object App extends IOApp {
  private val challengeParser = PlainTextChallengeParser(
    NonEmptySet.of("1", "2", "3", "4", "5", "6", "7", "8", "9"),
    NonEmptySet.of(" ")
  ).unsafeValue

  private val optionParser = OptionParser()

  private val solver = new Solver

  def run(args: List[String]): IO[ExitCode] = {
    val solved = for {
      options <- parseOptions(args)
      challenge <- loadChallenge(options)
      solution = solve(challenge, options)
    } yield solvedBoardMessage(solution, options)

    solved
      .handleErrorWith {
        case e: ApplicationFailure => IO.pure(usageInfo(e))
        case e                     => IO.raiseError(e)
      }
      .map(println)
      .map(_ => ExitCode.Success)
  }

  private def parseOptions(args: List[String]): IO[Options] =
    IO.fromEither(optionParser.parseOptions(args))

  private def loadChallenge(options: Options): IO[Challenge] =
    for {
      boardInPlainText <- loadBoardFile(options.boardFileName)
      challenge <- IO.fromEither(challengeParser.parse(boardInPlainText))
    } yield challenge

  private def solve(challenge: Challenge, options: Options): FailureOr[Solution] =
    solver.solve(challenge, options.steps)

  private def loadBoardFile(boardFile: String): IO[PlainTextChallenge] =
    fileResource(boardFile)
      .use { src =>
        IO(src.getLines().mkString(Properties.lineSeparator))
      }
      .map(PlainTextChallenge.apply)
      .handleErrorWith { exception =>
        IO.raiseError(InvalidArgumentFailure(exception.getMessage))
      }

  private def fileResource(fileName: String): Resource[IO, Source] =
    Resource
      .make(IO(Source.fromFile(fileName)))(src => IO(src.close()))

  private def solvedBoardMessage(solution: FailureOr[Solution], options: Options): UserMessage =
    solution match {
      case Right(solution)                        => boardSolved(solution)
      case Left(BoardAlreadySolved(board))        => boardWasAlreadySolved(board)
      case Left(BoardTooDifficult(board))         => boardTooDifficult(board, options)
      case Left(noMoreStepsLeft: NoMoreStepsLeft) => boardReached(noMoreStepsLeft)
      case Left(failure: InvalidArgumentFailure)  => boardNotSolved(failure, options)
    }

  private def boardWasAlreadySolved(boardToSolve: Board): UserMessage =
    um"The given board was already solved. Nothing to do here.${Properties.lineSeparator * 2}${showBoard(
      boardToSolve
    )}"

  private def boardSolved(solution: Solution): UserMessage =
    um"Board${Properties.lineSeparator * 2}${showBoard(
      solution.boardToSolve
    )}${Properties.lineSeparator * 2}was solved to${Properties.lineSeparator * 2}${showBoard(
      solution.solvedBoard
    )}${Properties.lineSeparator * 2}${difficultyNote(solution.difficulty)}"

  private def boardReached(noMoreStepsLeft: NoMoreStepsLeft): UserMessage =
    um"Board${Properties.lineSeparator * 2}${showBoard(
      noMoreStepsLeft.boardToSolve
    )}${Properties.lineSeparator * 2}was turned to${Properties.lineSeparator * 2}${showBoard(
      noMoreStepsLeft.reachedBoard
    )}${Properties.lineSeparator * 2}after ${noMoreStepsLeft.steps} steps. ${difficultyNote(noMoreStepsLeft.difficulty)}"

  private def difficultyNote(difficulty: Difficulty): String = difficulty match {
    case Difficulty.Easy      => "It was a breeze."
    case Difficulty.Medium    => "It required me some effort."
    case Difficulty.Difficult => "Uff, it was a difficult one."
  }

  private def boardTooDifficult(board: Board, options: Options): UserMessage =
    if (options.debug)
      um"Sorry, the given board was too difficult. I ended up here:${Properties.lineSeparator * 2}${showBoard(
        board
      )}${Properties.lineSeparator * 2}${showDebugBoard(board)}"
    else
      um"Sorry, the given board was too difficult. I ended up here:${Properties.lineSeparator * 2}${showBoard(
        board
      )}"

  private def boardNotSolved(failure: ApplicationFailure, options: Options): UserMessage =
    if options.debug then
      um"Sorry, there was an error with your input.${Properties.lineSeparator * 2}  ${failure.toDebugString}"
    else um"Sorry, there was an error with your input.${Properties.lineSeparator * 2}  $failure"

  private def showBoard(board: Board): String = {
    import de.fromm.sulver.board.instances.plaintext.*
    board.show
  }

  private def showDebugBoard(board: Board): String = {
    import de.fromm.sulver.board.instances.plaintext.debug.*
    board.show
  }

  private def usageInfo(failure: ApplicationFailure): UserMessage = UserMessage {
    s"""$failure
       |
       |  sulver [--debug] [--steps <number>] <boardFileName>
       |
       |The content format of the board file is:
       |
       |  +---+---+---++---+---+---++---+---+---+
       |  |   | 2 | 1 ||   |   | 9 ||   |   | 3 |
       |  +---+---+---++---+---+---++---+---+---+
       |  |   |   |   || 7 | 8 |   || 2 | 6 |   |
       |  +---+---+---++---+---+---++---+---+---+
       |  |   |   |   ||   |   |   || 7 |   |   |
       |  +---+---+---++---+---+---++---+---+---+
       |  +---+---+---++---+---+---++---+---+---+
       |  |   | 9 |   ||   |   |   || 3 | 1 |   |
       |  +---+---+---++---+---+---++---+---+---+
       |  |   |   | 5 ||   |   | 3 ||   | 4 |   |
       |  +---+---+---++---+---+---++---+---+---+
       |  |   |   |   || 8 |   |   ||   |   |   |
       |  +---+---+---++---+---+---++---+---+---+
       |  +---+---+---++---+---+---++---+---+---+
       |  |   |   | 3 ||   |   | 4 ||   | 8 |   |
       |  +---+---+---++---+---+---++---+---+---+
       |  |   |   |   || 5 |   |   || 6 |   |   |
       |  +---+---+---++---+---+---++---+---+---+
       |  |   |   |   || 6 |   |   ||   |   | 7 |
       |  +---+---+---++---+---+---++---+---+---+
       |""".stripMargin
  }
}
