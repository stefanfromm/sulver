package de.fromm.sulver.app

opaque type UserMessage = String

object UserMessage {

  def apply(value: String): UserMessage =
    value

  extension (self: UserMessage) {
    def value: String = self
  }
}

extension (sc: StringContext) {
  def um(args: Any*): UserMessage = sc.s(args*)
}
