package de.fromm.sulver.app

import de.fromm.sulver.util.ints.PositiveInt

case class Options(debug: Boolean, boardFileName: String, steps: PositiveInt)
