package de.fromm.sulver.failure

import de.fromm.sulver.board.Board
import de.fromm.sulver.game.Difficulty
import de.fromm.sulver.util.ints.PositiveInt

import java.io.{PrintWriter, StringWriter}
import scala.annotation.tailrec
import scala.util.control.NoStackTrace

sealed abstract class ApplicationFailure(val message: String, val cause: Option[ApplicationFailure] = None)
    extends Exception(message, cause.orNull),
      NoStackTrace {

  override def toString: String = allMessages.mkString(" ")

  def toDebugString: String = {
    val sw = StringWriter()
    printStackTrace(PrintWriter(sw))
    sw.toString
  }

  private def allMessages: List[String] = {
    @tailrec
    def go(failure: Option[ApplicationFailure], accumulator: List[String]): List[String] = failure match {
      case Some(f) => go(f.cause, accumulator :+ f.message)
      case None    => accumulator
    }

    go(Some(this), List.empty)
  }
}

case class InvalidArgumentFailure(override val message: String, override val cause: Option[ApplicationFailure] = None)
    extends ApplicationFailure(message, cause)

case class BoardTooDifficult(reachedUnsolvableBoard: Board) extends ApplicationFailure("The board is too difficult.")

case class BoardAlreadySolved(solvedBoard: Board) extends ApplicationFailure("The board is already solved.")

case class NoMoreStepsLeft(boardToSolve: Board, reachedBoard: Board, difficulty: Difficulty, steps: PositiveInt)
    extends ApplicationFailure("There are no more steps left.")
