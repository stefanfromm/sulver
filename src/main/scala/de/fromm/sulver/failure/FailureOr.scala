package de.fromm.sulver.failure

type FailureOr[A] = Either[ApplicationFailure, A]

object FailureOr {
  def right[A](a: A): FailureOr[A] = Right(a)
}
