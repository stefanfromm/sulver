package de.fromm.sulver.failure

import cats.effect.IO
import cats.syntax.either.*

implicit class FailureOrOps[A](val self: FailureOr[A]) extends AnyVal {
  def unsafeValue: A =
    self.valueOr(throw _)
}
