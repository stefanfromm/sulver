name := "sulver"

version := "0.1"

scalaVersion := "3.3.1"

enablePlugins(JavaAppPackaging)
maintainer := "stefanfromm@web.de"
Compile / mainClass := Some("de.fromm.sulver.app.App")

val scalaTestVersion = "3.2.17"
val catsVersion = "2.10.0"
val catsEffectVersion = "3.5.2"

libraryDependencies ++= Seq(
  "org.typelevel" %% "cats-core" % catsVersion,
  "org.typelevel" %% "cats-effect" % catsEffectVersion,
  "org.scalatest" %% "scalatest-flatspec" % scalaTestVersion % Test,
  "org.scalatest" %% "scalatest-shouldmatchers" % scalaTestVersion % Test
)

scalacOptions ++= Seq(
  // https://nathankleyn.com/2019/05/13/recommended-scalac-flags-for-2-13/
  // https://docs.scala-lang.org/scala3/guides/migration/options-new.html
  // todo: find flag list for Scala 3
  "-deprecation", // Emit warning and location for usages of deprecated APIs.
  "-feature", // Emit warning and location for usages of features that should be imported explicitly.
  "-language:existentials", // Existential types (besides wildcard types) can be written and inferred
  "-language:experimental.macros", // Allow macro definition (besides implementation and application)
  "-language:higherKinds", // Allow higher-kinded types
  "-language:implicitConversions", // Allow definition of implicit functions called views
  "-unchecked", // Enable additional warnings where generated code depends on assumptions.
  "-Xfatal-warnings", // Fail the compilation if there are any warnings.
  "8" // Enable parallelisation — change to desired number!
)
