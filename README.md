# Sulver

A simple Sudoko solver which is written in the functional style. It is not able to solve any Sudoko challenge, but has built in some more advanced solving rules. It gives up where a search algorithm would be needed to solve a Sudoko board by trying alternative solution paths.

Only the main class `App` contains non-functional code, because it handles I/O. Nevertheless a lot of methods in `App` are still real functions. Only the `main` method is not functional :)



# Build

See https://www.scala-sbt.org/sbt-native-packager/archetypes/java_app/index.html.

## Staged Package

```shell
sbt clean test stage
```

creates an executable application at

```shell
target/universal/stage
```

## ZIP archive

```shell
sbt clean test Universal/packageBin
```

creates an archive

```shell
target/universal/sulver-<version>.zip
```



# Run

Simple command shell:

```shell
$ cd target/universal/stage/bin
$ sulver [--debug] <sudoku-board-input-file-name>
```

Powershell
```shell
$ cd target/universal/stage/bin
.\sulver  [--debug] "<sudoku-board-input-file-name>"
```

There are test boards available in the folder `testdata`.



# Domain Model

## Symbols

- Code: a symbol used to fill in a cell on a Sudoko board. By default the codes 1 to 9 are used on a board of height and
  width 9.

## Geometry

- Position: a 0-based position on the board, e.g. the position of a row or a column. At least 0.
- Length: a length, e.g. the board height and width. At least 1.
- Coordinates: a pair of coordinates e.g. of a cell. Coordinates have a position for the column and a position for the
  row.

The geometry classes enable type-safety. You cannot use positions where you work with lengths. The math behind the
geometry classes is safe. E.g. it ensures that a position will always result in a valid position. This is achieved by
hiding the constructors of these classes. Instead there are offered fully functional smart constructors in their
companion objects. Once you have obtained an instance of a geometry class you can derive other instances of geometry
classes only in safe ways. E.g. `Position` has the method `next` which returns the next position. This in turn is safe
as it is guaranteed that the derived position is still at least 0.

## Boards

- Board: a Sudoku board which contains of cells.
- Cell: a cell at a given pair of coordinates. For a cell there is either a presolved code or there is going to be found
  a code.
- Box: a subboard of a board. E.g. on a 9x9 board there are 3x3 boxes of height and width 3.
- Column: a column at a given position across the whole board.
- Row: a row at a given position across the whole board.

## Challenge

- Challenge: an unsolved board with some presolved cells.
- PresolvedCell: a cell where the code is known in advance.

## Game

- Solution: a fully solved board.
- Move: a move which results in a new board if applied to a given board.
- Solver: the heart of the Sudoku solver which finds applicable moves and applies them to boards in order to find a
  solution.

# Lessons Learned

1. Case classes allow for easy description of immutable data structures. Because of they are fully initialized by
   constructors, they do NOT allow for bidirectional references. In our case a board has a set of cells, but a cell
   CANNOT have a backreference to its board. This implies that we cannot ask the cell for its neighbours. For this we
   need to ask the board in the name of the cell.
    1. In a first approach I used path-dependent types which I thought could ensure that a cell given to a method like
       e.g. `solveCell(cell: Cell, code: Code): Board` would originate from a given board. It turned out that
       path-dependent types do NOT work well together with functional style, because in the following for-comprehension:
       ```scala
       for {
         board <- ...
         cell <- board.cellAt(...)
         nextBoard = board.solveCell(cell, ...)
         ...
       } yield ...
       ```
       the Scala compiler CANNOT ensure that the obtained cell originates from the given board and thus disallows to
       call the line `board.solveCell(cell)`.
    2. So in a second approach I turned `Cell` into a top-level class. This changed the method signature
        to `solveCell(cell: Cell, code: Code): FailureOr[Board]` and requires us to call it like this:
        ```scala
        for {
          board <- ...
          cell <- board.cellAt(...)
          nextBoard <- board.solveCell(cell, ...)
          ...
        } yield ...
        ```
        The difference is minimal in the calling code. Only the implementation of the method `solveCell()` is more
        complicated as it contains validation of the origin of the given cell.
2. Instead of writing `Either[ApplicationFailure, A]` I introduced the type `FailureOr[A]`. It shortens code and improves
   readability. But it generalizes the error class to the top-level. This is not a big deal for this program, as in fact
   there can be only user input errors. But could we distinguish user input failures from temporary unavailability
   failures and handle them differently based on the method signature?
3. Later in the project I decided to let `ApplicationFailure` inherit from `Exception` in order to produce stack traces
   in the debug mode. This effectively brings `FailureOr` closer to `Try`. I'm not really sure which one of them to
   prefer.
4. Stack-safe recursive algorithms in monad-based algorithms are much harder. Please see the method `Solver.solve` which
   takes advantage of Scala Cats' `Eval` monad to accomplish this in an acceptable way. The reason is that in functional
   style programs we sequence operations by `flatMap`. And thus the recursive call is never in tail-recursive position.
   Please compare the monad-based approach with the simplicity of a non-monad-based approach of a tail-recursive,
   stack-safe recursion e.g. in `ApplicationFailure.allMessages`.
5. Scala Cats was quite useful even for this simple project.
   1. `NonEmptyList` and `NonEmptySet` are used everywhere where `List` and `Set` would be too loose. The non-empty classes do NOT have all the methods of their possibly empty counterparts. Sometimes it felt cumbersome to unpack and pack collections. For `NonEmptySet` you need to implement `cats.Order` while for `SortedSet` you need to provide `scala.Ordered`. Fortunately it is possible to derive them in both directions. So you need to implement only one of them. See `Cell.ordering` vs `Cell.order`.
   2. I tried out `cats.Show` for implementing different board representations for the debug mode and the normal mode.
   3. `cats.Eval` helped me to add trampolining to recursive monad-based algorithms in order to make them stack-safe.
   4. `cats.syntax.traverse.*` is something you'll need for sure when working with functional results like `Try` and `Either` and your functions handle instances of `Option` or `List`. With `traverse` you can easily turn e.g. a `List[FailureOr[A]]` into a `FailureOr[List[A]]`. 
   4. Cats offers some useful methods for `Either` like e.g. `valueOr` in `cats.syntax.either.*`. I use this in `FailureOrOps.unsafeValue`.
6. In Scala-based command line tools there is no need to write argument parsers. It is so easy to accomplish this by pattern matching. See `App.preparseOptions`.
7. I managed to upgrade from Scala 2.13 to Scala 3.1. It is great to define types like `FailureOr` just as a top-level type without the need to embed it into an object. I also tried out the new if-then-else syntax, because I wanted to see if the decreased amount of parentheses improves readability.